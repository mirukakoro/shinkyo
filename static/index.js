let token = "";
let tokenExpire = "";

function onLoad() {
    jumpToPage('signin');
}

function jumpToPage(page) {
    $('#pages').children().hide();
    $('#page-' + page.toString()).show();
    if (page === 'signin') {
        console.log('signin show')
        $('#page-signin-bg').show();
    } else {
        console.log('signin hide')
        $('#page-signin-bg').hide();
    }
}

function pageSigninAttempt() {
    $("#page-signin-status").text("Signing in...");
    document.getElementById("page-signin-status").innerText = "Signing in...";
    const username = $("#page-signin-username").val()
    const password = $("#page-signin-password").val()
    const url = new URL('http://localhost:8080/api/user/auth');
    const params = {username: username.toString(), password: password.toString()};
    url.search = new URLSearchParams(params).toString();
    fetch(url)
    .then(function(response) {
        let data = response.json();
        alert(data["code"]);
        if (data["code"] === 4011) {
            $("#page-signin-status").text("Incorrect username.");
        } else if (data["code"] === 4012) {
            $("#page-signin-status").text("Incorrect password.");
        } else if (data["code"] === 2001) {
            $("#page-signin-status").text("Successfully signed in.");
        } else {
            $("#page-signin-status").text(
                "Error occurred while contacting server to sign in: status code is " +
                data["code"].toString());
        }
        token = data["token"]
        tokenExpire = data["token_expire"]
    })
    .catch(function(err) {
        alert(err);
        $("#page-signin-status").text("Error occurred while contacting server to sign in: " + err.toString());
    });
}
