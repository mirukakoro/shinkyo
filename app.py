import datetime
import os
import traceback

import toml
from flask import Flask, render_template, request, jsonify

import database


def log_exit(code_: int):
  print(f'Exiting with code {code_}.')
  exit(code_)

print('Reading config from `config.toml`...')
try:
  with open('config.toml', 'r') as file:
    config = toml.load(file)
except FileNotFoundError as error:
  print(f'`config.toml` was not found. {traceback.format_exc()}')
  log_exit(14)
print('Read config from `config.toml`.')

production_ = os.environ.get('PRODUCTION', None)
if production_ == None or production_ == '0' or production_.lower() == 'false':
  production = False
else:
  production = True

app = Flask(__name__)
app.config['SECRET_KEY'] = b'\xc3\x0e\xd63\x1a\x08\x02^\xcd\xa1\xc2\xe8m\x171\xbf'
print('Applying config...')
for key, value in config['flask']['config'].items():
  app.config[key] = value
for key, value in config['environ'].items():
  os.environ[key] = value
print('Applied config.')
app.config.from_object(__name__)
if production:
  raise RuntimeError('production not supported')
else:
  db = database.init_app(app, db_uri = config.get('db_uri', None))


@app.teardown_appcontext
def teardown_appcontext(exception):
  db.session.commit()
  db.session.close()


def gen_resp(code: int, msg: str) -> dict:
  return {'code': code, 'msg': msg}

@app.route('/')
def index():
  return render_template(
    'app.html',
    nowtime = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc),
    persons = config['persons'],
    captions = config['captions'],
    courses = config['courses'],
    chat_topics = config['chat_topics'],
    chat_msgs = config['chat_msgs'],
  )

@app.route('/api/post/get')
def post_get():
  resp = gen_resp(501, 'not implementted yet')
  return resp, 501

@app.route('/api/post/new')
def post_new():
  resp = gen_resp(501, 'not implementted yet')
  return resp, 501

@app.route('/api/inst/get')
def inst_get():
  resp = gen_resp(501, 'not implementted yet')
  return resp, 501

@app.route('/api/inst/new')
def inst_new():
  resp = gen_resp(501, 'not implementted yet')
  return resp, 501

@app.route('/api/user/auth')
def user_auth():
  missing = []
  if 'username' not in request.args.keys():
    missing.append('username')
  if 'password' not in request.args.keys():
    missing.append('password')
  if len(missing) > 0:
    resp = gen_resp(400, f'must have args: {missing}')
    return jsonify(resp), 400
  print(request.args.get('username'), database.User.query.all())
  user_exists = database.User.query.filter_by(username = request.args.get('username')).scalar() is not None
  if user_exists:
    user = database.User.query.filter_by(username = request.args.get('username')).first()
    if user.check_password(password = request.args.get('password')):
      resp = gen_resp(2001, 'signin ok')
      token, time_start, time_delta = database.gen_token(request.args.get('username'))
      db.session.add(token)
      db.session.commit()
      resp['token'] = token.token
      resp['token_expire'] = (time_start + time_delta).isoformat()
      return jsonify(resp), 200
    else:
      resp = gen_resp(4012, 'password bad')
      return jsonify(resp), 200
  else:
    resp = gen_resp(4011, 'username bad')
    return jsonify(resp), 200

@app.route('/api/teapot')
def teapot():
  resp = gen_resp(418, '''I'm a teapot!

               ;,'
     _o_    ;:;'
 ,-.'---`.__ ;
((j`=====',-'
 `-\     /
    `-=-'     hjw
Teapot by Hayley Jane Wakenshaw
https://www.asciiart.eu/food-and-drinks/coffee-and-tea''')
  return jsonify(resp)

def make_error_handler(code):
  def error_handler(e):
    resp = gen_resp(code, str(e))
    return resp, code
  
  return error_handler


codes = [400, 401, 403, 404, 410, 500, 501]

for code in codes:
  app.register_error_handler(code, make_error_handler(code))

if __name__ == '__main__':
  app.run(host = '0.0.0.0', port = 8080)
