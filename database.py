import datetime
import secrets

import flask
import werkzeug.security
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def gen_password(password: str, method: str, salt_length: int) -> str:
  """
  Generates new password for this User instance.
  """
  return werkzeug.security.generate_password_hash(password, method = method, salt_length = int(salt_length))


class User(db.Model):
  __tablename__ = 'users'
  username = db.Column(db.String(128), primary_key = True)
  email = db.Column(db.String(128), unique = True)
  hash = db.Column(db.String(20000))
  group = db.Column(db.String(1024), nullable = False)
  name = db.Column(db.String(1024), nullable = False)
  institution_id = db.Column(db.String(1024), nullable = True)
  notes = db.Column(db.String(2000000), nullable = False)
  
  def __repr__(self):
    return f'<User {self.username}>'
  
  def check_password(self, password: str) -> bool:
    """
    Return true if password is correct, false if not.
    """
    return werkzeug.security.check_password_hash(self.hash, password)
  
  def gen_password(self, method: str, salt_length: int) -> None:
    """
    Generates new password for this User instance.
    """
    self.hash = werkzeug.security.generate_password_hash(self.password, method = method, salt_length = salt_length)
    db.session.add(self)
    db.session.commit()
  
  @property
  def friendlyrepr(self) -> str:
    return f'{self.name} / {self.username} ({self.group})'
  
  @property
  def shortrepr(self) -> str:
    return f'{self.name} / {self.username}'


class Institution(db.Model):
  id = db.Column(db.String(36), primary_key = True, nullable = False)
  name = db.Column(db.String(20000), nullable = False)
  location = db.Column(db.String(20000), nullable = False)
  
  def __repr__(self):
    return f'<Institution {self.action_uid}>'


class Post(db.Model):
  id = db.Column(db.String(36), primary_key = True, nullable = False)
  parent = db.Column(db.String(36), nullable = True)
  content = db.Column(db.String(200000000), nullable = False)
  
  def __repr__(self):
    return f'<Post {self.id}>'

class Token(db.Model):
  token = db.Column(db.String(20000), primary_key = True, nullable = False)
  username = db.Column(db.String(128), nullable = False)
  time_start = db.Column(db.String(128), nullable = False) # created time
  time_delta = db.Column(db.String(128), nullable = False) # duration
  
  def __repr__(self):
    return f'<Token for {self.username}>'

def gen_token(username: str, delta: datetime.timedelta = datetime.timedelta(hours = 1)) -> Token:
  token = secrets.token_urlsafe(256) # TODO: Check if longer string causes no issues w/ lag
  start = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)
  return Token(
    token = token,
    username = username,
    time_start = start.isoformat(),
    time_delta = str(delta),
  ), start, delta

def init_app(app: flask.Flask, db_uri: str) -> SQLAlchemy:
  app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
  db.init_app(app)
  db.create_all(app = app)
  # with app.app_context():
  #   user = User(
  #     username = 'test',
  #     email = 'test@example.com',
  #     hash = gen_password(
  #              'testpassword',
  #              'pbkdf2:sha256:1000000',
  #              1000000,
  #            ),
  #     group = 'admin',
  #     name = 'Test User',
  #     institution_id = None,
  #     notes = 'Test user.'
  #   )
  #   db.session.add(user)
  #   db.session.commit()
  return db
